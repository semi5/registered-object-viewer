export function setup(ctx) {
    // cache data
    let gameMaps;
    let renderItems;

    const VERSION = "1.1.2";

    const variablePath = [];

    // map finder
    const getObjectContainsMap = (obj) => {
        return Object.values(obj).some(value => value instanceof NamespaceRegistry);
    };

    const getRegisteredMaps = (obj, doDepthSearch = false) => {
        let registerMaps = [];
        Object.keys(obj).forEach(key => {
            let map = obj[key];
            
            if(map == null)
                return;
            
            if (map instanceof NamespaceRegistry) {
                registerMaps.push([key, map]);
            }
            else if(doDepthSearch) {
                if(getObjectContainsMap(map)) {
                    registerMaps.push([key, map]);
                }
            }
        });
        
        registerMaps = registerMaps.sort((a, b) => {
            return a[0].localeCompare(b[0]);
        });
        
        return registerMaps;
    };

    // modal
    const showModal = () => {
        updateModal();
        updateVariablePath(0, 'game');
        $('#modal-registration-viewer').modal('show');
    };

    const injectModal = () => {
        if ($(`#modal-registration-viewer`).length) {
            $(`#modal-registration-viewer`).remove();
        }
        $('#modal-privacy').after(`
            <div class="modal" id="modal-registration-viewer" tabindex="-1" role="dialog" aria-labelledby="modal-block-normal" aria-hidden="true" style="display: none;">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <div class="block block-themed block-transparent mb-0">
                            <div class="block-header bg-primary-dark">
                                <h3 class="block-title">Registered Object Viewer v${VERSION}</h3>
                                <div class="block-options">
                                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-fw fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="block-content pt-0 font-size-sm">
                                <div class="pb-2">
                                    Current Variable Path: <kbd id="registration-viewer-path"></kbd>
                                </div>
                                <div>
                                    <ul class="nav-main nav-main-horizontal nav-main-horizontal-override font-w400 font-size-sm mb-1" id="registration-viewer-primary"></ul>
                                </div>
                                <div class="pb-4" id="registration-viewer-container"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`);

        $('#modal-registration-viewer').on('hidden.bs.modal', () => {
            clearContainer();
        });
    };

    const updateModal = () => {
        gameMaps = getRegisteredMaps(game, true);
        buildNavList('registration-viewer-primary', gameMaps, onPrimaryClick);
        $('#registration-viewer-secondary').remove();
        $('#registration-viewer-container').empty();
    };

    const buildNavList = (parent_id, items, callback) => {
        let nav = $(`#${parent_id}`);
        nav.empty();

        items.forEach((item) => {
            let navItem = $(`<li class="mr-1 mb-1"></li>`);
            let navButton = $(`<button class="btn btn-sm btn-outline-success"><small>${item[0]}</small></button>`);
            navButton.click(evt => callback(evt, item));
            navItem.append(navButton);
            nav.append(navItem);
        });
    };

    const updateNav = (evt, parent_id) => {
        let navList = $(`#${parent_id} .btn-success`);
        navList.removeClass('btn-success');
        navList.addClass('btn-outline-success');

        let navItem = $(evt.currentTarget);
        navItem.removeClass('btn-outline-success');
        navItem.addClass('btn-success');
    };

    const variablePathAppend = (text) => text + (text == 'allObjects' ? '' : '.allObjects');
    const updateVariablePath = (index, text) => {
        variablePath.length = index;
        variablePath[index] = text;
        $('#registration-viewer-path').text(variablePath.join("."));
    };

    const onPrimaryClick = (evt, item) => {
        updateNav(evt, 'registration-viewer-primary');
        clearContainer();

        let [key, obj] = item;

        let subMaps = getRegisteredMaps(obj);
        if (subMaps.length > 0) {
            if (obj.registeredObjects != null) {
                subMaps.unshift(['allObjects', obj]);
            }
            updateVariablePath(1, item[0]);
            $('#registration-viewer-primary').after(`<ul class="nav-main nav-main-horizontal nav-main-horizontal-override font-w400 font-size-sm border-top py-2" id="registration-viewer-secondary"></ul>`);
            buildNavList('registration-viewer-secondary', subMaps, onSecondaryClick);
        }
        else {
            updateVariablePath(1, variablePathAppend(item[0]));
            renderMap(item);
        }
    };

    const onSecondaryClick = (evt, item) => {
        updateNav(evt, 'registration-viewer-secondary');
        updateVariablePath(2, variablePathAppend(item[0]));

        $('#registration-viewer-container').empty();
        renderMap(item);
    };

    const clearContainer = () => {
        $('#registration-viewer-secondary').remove();
        $('#registration-viewer-container').empty();
    };

    const ALL_ITEM_FIELDS = ['media', 'name', 'id', 'category', 'type', 'isModded'];
    const renderMap = (item) => {
        let [key, map] = item;

        renderItems = map.allObjects;

        if (renderItems == null || renderItems.length <= 0) {
            return;
        }

        // get current fields
        const CURRENT_FIELDS = [];
        ALL_ITEM_FIELDS.forEach(field => {
            if (renderItems[0][field] != null) {
                CURRENT_FIELDS.push(field);
            }
        });

        $('#registration-viewer-container').append($(`<table class="table table-sm table-vcenter">
            <thead><tr>${CURRENT_FIELDS.map(header => transformTableHeader(renderItems[0], header)).join('')}</tr></thead>
            <tbody>${renderItems.map(renderItem => transformTableRow(renderItem, CURRENT_FIELDS)).join('')}</tbody>
        </table>`));
    };

    const transformTableHeader = (row, header) => {
        switch (header) {
            case 'media':
                return `<th style="width: 32px;">${header}</th>`;
            case 'category':
                if(row.category.id != null)
                    return `<th>category.id</th>`;
                return `<th>category</th>`;
        }
        return `<th>${header}</th>`;
    };

    const transformTableRow = (row, CURRENT_FIELDS) => {
        return `<tr>${CURRENT_FIELDS.map(FIELD => transformTableValue(row, FIELD)).join('')}</tr>`;
    };

    const transformTableValue = (row, field) => {
        switch (field) {
            case 'media':
                return `<td><img class="resize-32" src="${row.media}" /></td>`;
            case 'category':
                if(row.category.id != null)
                    return `<td class="font-size-sm">${row.category.id}</td>`;
                return `<td class="font-size-sm">${row.category}</td>`;
        }
        return `<td class="font-size-sm">${row[field]}</td>`;
    };

    // hooks + game patches
    ctx.onInterfaceReady(() => {
        injectModal();
        
        sidebar.category('Other').item('Registered Object Viewer', {
            iconClass: 'd-none',
            nameClass: 'font-size-sm',
            name: createElement('small', {
                children: [
                    document.createTextNode('Registered Object Viewer'),
                ],
            }),
            onClick: () => showModal()
        });
    });
}
